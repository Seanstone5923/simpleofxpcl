project(ofxApp)
cmake_minimum_required(VERSION 2.8)

# OpenFrameworks path
#set(OF_PATH ${CMAKE_SOURCE_DIR}/../of_v0.9.3_linux64_release)
set(OF_PATH /opt/openFrameworks)

# Run OpenFrameworks CMake script
include("${CMAKE_CURRENT_SOURCE_DIR}/FindOpenFrameworks.cmake")

# Flags
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -pthread")
set(POCO_STATIC)

# Include dirs
include_directories(${CMAKE_SOURCE_DIR})

# Collect source files
file(GLOB SRC_LIST "*.cpp")
#message(${SRC_LIST})

# Executable
add_library(ofxApp ${SRC_LIST})

# Packages
find_package(Freetype REQUIRED)
find_package(GLEW REQUIRED)
find_package(OpenGL REQUIRED)
find_package(Boost COMPONENTS system filesystem REQUIRED)
find_package(X11 REQUIRED)
find_package(OpenAL REQUIRED)
include("${CMAKE_CURRENT_SOURCE_DIR}/FindGStreamer.cmake")
include_directories(${GSTREAMER_INCLUDE_DIRS})
include("${CMAKE_CURRENT_SOURCE_DIR}/FindCairo.cmake")
include_directories(${CAIRO_INCLUDE_DIRS})
#include("${CMAKE_CURRENT_SOURCE_DIR}/Findmpg123.cmake")
#include("${CMAKE_CURRENT_SOURCE_DIR}/FindFontconfig.cmake")

# Linking
target_link_libraries(ofxApp
    openFrameworks
    ssl crypto PocoNetSSL PocoCrypto PocoNet PocoZip PocoUtil PocoXML PocoJSON PocoFoundation
    fmodex
    glfw3
    kiss
    tess2
    ${CAIRO_LIBRARIES}
    ${FREEIMAGE_LIBRARIES}
    ${FREETYPE_LIBRARIES} fontconfig #${FONTCONFIG_LIBRARIES}
    ${GLEW_LIBRARIES}
    ${OPENGL_LIBRARIES}
    ${OPENAL_LIBRARY} mpg123 sndfile #${MPG123_LIBRARIES}
    ${Boost_SYSTEM_LIBRARY} ${Boost_FILESYSTEM_LIBRARY}
    ${X11_LIBRARIES} Xcursor Xrandr Xi Xxf86vm
)
