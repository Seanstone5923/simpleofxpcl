## Macro to list subdirs
MACRO(SUBDIRLIST result curdir)
  FILE(GLOB children RELATIVE ${curdir} ${curdir}/*)
  SET(dirlist "")
  FOREACH(child ${children})
    IF(IS_DIRECTORY ${curdir}/${child})
      LIST(APPEND dirlist ${child})
    ENDIF()
  ENDFOREACH()
  SET(${result} ${dirlist})
ENDMACRO()
################################################################################

## FreeImage
include("${CMAKE_CURRENT_SOURCE_DIR}/FindFreeImage.cmake")

## OpenFrameworks include paths

set(SUBDIRS)
SUBDIRLIST(SUBDIRS ${OF_PATH}/libs)
foreach(subdir ${SUBDIRS})
    if (EXISTS "${OF_PATH}/libs/${subdir}/include")
        message("Include:\tlibs/${subdir}/include")
        include_directories(BEFORE ${OF_PATH}/libs/${subdir}/include)
    endif()
endforeach()

message("Include:\tlibs/openFrameworks")
include_directories(BEFORE ${OF_PATH}/libs/openFrameworks)

set(SUBDIRS)
SUBDIRLIST(SUBDIRS ${OF_PATH}/libs/openFrameworks)
foreach(subdir ${SUBDIRS})
    if (EXISTS "${OF_PATH}/libs/openFrameworks/${subdir}")
        message("Include:\tlibs/openFrameworks/${subdir}")
        include_directories(BEFORE ${OF_PATH}/libs/openFrameworks/${subdir})
    endif()
endforeach()

## Link paths
link_directories("${OF_PATH}/libs/openFrameworksCompiled/lib/linux64")
link_directories("${OF_PATH}/libs/fmodex/lib/linux64/")
link_directories("${OF_PATH}/libs/glfw/lib/linux64/")
link_directories("${OF_PATH}/libs/kiss/lib/linux64/")
link_directories("${OF_PATH}/libs/poco/lib/linux64/")
link_directories("${OF_PATH}/libs/tess2/lib/linux64/")
