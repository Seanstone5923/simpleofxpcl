#include "ofApp.h"

//========================================================================
int ofx_main( )
{
	// OpenFrameworks
	ofGLWindowSettings s;
	s.setGLVersion(3,2);
	ofCreateWindow(s);

	// this kicks off the running of my app
	// can be OF_WINDOW or OF_FULLSCREEN
	// pass in width and height too:
	ofRunApp(new ofApp());
}
